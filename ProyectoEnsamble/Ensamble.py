import sys
from PyQt5 import QtWidgets, uic, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import *
import matplotlib.pyplot as plt
from numpy import arange,sin,pi
import tkinter.filedialog
import pandas as pd
import numpy as np
import time
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import Perceptron
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score




class EjGUI(QtWidgets.QMainWindow):
    names=[]
    rutaArchivo=""
    str_cov_mat=""
    str_eig_vals=""
    str_eig_vecs=""
    score_KNN=""
    score_NB=""
    score_NN=""
    num_elementos=0
    num_atributos=0
    num_clases=0
    df = pd.DataFrame
    X_train = pd.DataFrame
    X_test = pd.DataFrame
    y_train = pd.DataFrame
    y_test = pd.DataFrame
    var_exp=[0]
    eig_pairs=[0]
    X=np.ndarray(0)
    y=np.ndarray(0)
    X_std=np.ndarray(0)
    cum_var_exp=np.ndarray(0)
    model_1=KNeighborsClassifier
    model_2=GaussianNB
    model_3=MLPClassifier

    def __init__(self):
        super().__init__()
        uic.loadUi("MainWindow.ui", self)
        pixmap = QPixmap("imagenPrincipal1.png")
        pixmap2 = QPixmap("Recursos/ensamble2.png")
        self.label_imagen.setPixmap(pixmap)
        self.label_imagenClasifi.setPixmap(pixmap)
        w=self.label_imagenEnsam.width()
        h=self.label_imagenEnsam.height()
        self.label_imagenEnsam.setPixmap(pixmap2.scaled(w,h));
        self.btn_abrirArchivo.setEnabled(True)
        self.btn_abrirArchivo.clicked.connect(self.btn_abrir)
        self.btn_calcularPCA.clicked.connect(self.btn_PCA)
        self.btn_matrizCov.clicked.connect(self.btn_matriz_cov)
        self.btn_autovalores.clicked.connect(self.btn_autVal)
        self.btn_autovectores.clicked.connect(self.btn_autVec)
        self.btn_graficaVarianza.clicked.connect(self.btn_graficaVar)
        self.btn_graficaPCA.clicked.connect(self.btn_grafPCA)
        self.btn_KNN.clicked.connect(self.btn_habilitarKNN)
        self.btn_calcularKNN.clicked.connect(self.btn_calcKNN)
        self.btn_naiveBayes.clicked.connect(self.btn_calcularNaive)
        self.btn_neuralNetwork.clicked.connect(self.btn_neural)
        self.btn_calcularNeural.clicked.connect(self.btn_calcularNN)
        self.btn_ensamble.clicked.connect(self.btn_calcularEnsamble)
        self.btn_graficaVarianza.setEnabled(False)
        self.btn_calcularPCA.setEnabled(False)
        self.btn_matrizCov.setEnabled(False)
        self.btn_autovalores.setEnabled(False)
        self.btn_autovectores.setEnabled(False)
        self.btn_graficaVarianza.setEnabled(False)
        self.btn_graficaPCA.setEnabled(False)
        self.btn_KNN.setEnabled(False)
        self.btn_naiveBayes.setEnabled(False)
        self.btn_neuralNetwork.setEnabled(False)
        self.btn_ensamble.setEnabled(False)
        self.btn_calcularKNN.hide()
        self.btn_calcularNeural.hide()
        self.spinBox_numKNN.hide()
        self.spinBox_epocas.hide()
        self.doubleSpinBox_momentum.hide()
        self.label_momentum.hide()
        self.btn_errorKNN.hide()
        self.label_3.hide()
        self.label_4.hide()

    def btn_abrir(self):
        global rutaArchivo
        global num_elementos
        global num_atributos
        global num_clases
        global score_KNN
        global score_NB
        global score_NN
        global df
        global names
        self.plain_resultadoPCA.clear()
        self.plain_resultClasifi.clear()
        self.plain_resultEnsam.clear()
        self.label_nombreArchivo.clear()
        self.label_tituloPCA.clear()
        self.label_tituloClasifi.clear()
        self.label_tituloEnsamble.clear()
        self.label_KNN.clear()
        self.label_NB.clear()
        self.label_NN.clear()
        self.label_nombreArchivo.setText("Archivo cargado: ")
        root = tkinter.Tk() #esto se hace solo para eliminar la ventanita de Tkinter 
        root.withdraw() #ahora se cierra       
        root.filename = tkinter.filedialog.askopenfilename(filetypes=[("default", "*.txt")]) # Muestra y abre el explorador de archivos, al mismo tiempo regresa el path del archivo
        rutaArchivo = str(root.filename)
        #se almacena en la variable "rutaArchivo"
        if(rutaArchivo!=""):
            self.btn_calcularPCA.setEnabled(True)
            self.btn_ensamble.setEnabled(True)
            score_KNN=""
            score_NB=""
            score_NN=""
            self.label_nombreArchivo.setText("Archivo cargado: "+rutaArchivo)
            self.label_tituloPCA.setText("Archivo cargado correctamente")
            self.label_tituloClasifi.setText("No se ha calculado PCA")
            self.label_tituloEnsamble.setText("Archivo cargado correctamente")
            i=0
            #leer el archivo y alamacenar en tres variables el numero de clases, atributos y elementos,
            #para después poder utilizarlos.
            f = open(rutaArchivo, "r")
            while(i < 3):
                linea = f.readline()
                if(i==0):
                    num_ele=linea
                    num_elementos=int(num_ele)
                elif(i==1):
                    num_atri=linea
                    num_atributos=int(num_atri)
                elif(i==2):
                    num_clas=linea            
                    num_clases=int(num_clas)
                if not linea:
                    break
                i=i+1
            names = []
            for i in range (0, num_atributos):
                names.append("Atributo "+str(i+1))
            names.append('Clase')
            df = pd.read_csv(f, names=names) #guardar los elementos del archivo (ya sin las primeras tres lineas).
            f.close() #cerrar archivo
            self.label_numElem.setText("Número de elementos: "+ num_ele)
            self.label_numAtri.setText("Número de atributos: "+ num_atri)
            self.label_numClases.setText("Número de clases: "+ num_clas)
            self.label_numElem_2.setText("Número de elementos: "+ num_ele)
            self.label_numAtri_2.setText("Número de atributos: "+ num_atri)
            self.label_numClases_2.setText("Número de clases: "+ num_clas)
            self.label_numElem_3.setText("Número de elementos: "+ num_ele)
            self.label_numAtri_3.setText("Número de atributos: "+ num_atri)
            self.label_numClases_3.setText("Número de clases: "+ num_clas)
            self.btn_graficaVarianza.setEnabled(False)
            self.btn_matrizCov.setEnabled(False)
            self.btn_autovalores.setEnabled(False)
            self.btn_autovectores.setEnabled(False)
            self.btn_graficaVarianza.setEnabled(False)
            self.btn_graficaPCA.setEnabled(False)
            self.btn_KNN.setEnabled(False)
            self.btn_naiveBayes.setEnabled(False)
            self.btn_neuralNetwork.setEnabled(False)
            self.spinBox_numComponentes.setMaximum(num_atributos)
        
    def btn_PCA(self):
        self.plain_resultadoPCA.clear()
        global num_elementos
        global num_atributos
        global num_clases
        global str_cov_mat
        global str_eig_vals
        global str_eig_vecs
        global var_exp
        global df
        global eig_pairs
        global X
        global y
        global X_std
        global cum_var_exp
        global X_train
        global X_test
        global y_train
        global y_test
        global names
        # Se divide la matriz del dataset en dos partes
        X = df.drop('Clase', 1)
        #La submatriz x contiene los valores de las columnas del dataframe y todas las filas
        y = df['Clase']
        # El vector y contiene los valores de la última columna(clase) para todas las filas
        #Aplicar una transformación de los datos para poder aplicar las propiedades de la distribución normal
        X_std = StandardScaler().fit_transform(X)
        #Aplicar PCA
        num_comp = self.spinBox_numComponentes.value() #obtener el numero de componentes principales
        pca = PCA(n_components=num_comp)
        principalComponents = pca.fit_transform(X_std)
        
        componentes = []
        for i in range(num_comp):
            componentes.append('PC'+ str(i+1))
        
        principalDf = pd.DataFrame(data = principalComponents, columns = componentes) #guardar los nuevos datos, una vez aplicado PCA
        finalDf = pd.concat([principalDf, df[['Clase']]], axis = 1)
        #mostrar la tabla de los componentes principales
        self.plain_resultadoPCA.setPlainText("\n\n"+finalDf.to_string())
        X = finalDf.drop('Clase', 1)
        y = finalDf['Clase']
        
        #Hacer el entrenamiento de datos, para después utilizarlos en los clasificadores
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)
        
        # Calcular la matriz de covarianza
        cov_mat = np.cov(X_std.T)
        str_cov_mat=np.array2string(cov_mat, formatter={'float_kind':lambda cov_mat: "%.10f" % cov_mat})
        #Calcular los autovalores y autovectores de la matriz y los mostramos
        eig_vals, eig_vecs = np.linalg.eig(cov_mat)
        str_eig_vals=np.array2string(eig_vals, formatter={'float_kind':lambda eig_vals: "%.10f" % eig_vals})
        str_eig_vecs=np.array2string(eig_vecs, formatter={'float_kind':lambda eig_vecs: "%.10f" % eig_vecs})

        #Hacer una lista de parejas (autovector, autovalor) 
        eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]

        #Ordenar estas parejas en orden descendiente con la función sort
        eig_pairs.sort(key=lambda x: x[0], reverse=True)
        #Lista de autovalores en orden desdencientes
        tot = sum(eig_vals)
        var_exp = [(i / tot)*100 for i in sorted(eig_vals, reverse=True)]
        cum_var_exp = np.cumsum(var_exp)
        
        self.label_tituloPCA.setText("PCA calculado correctamente")
        self.btn_matrizCov.setEnabled(True)
        self.btn_autovalores.setEnabled(True)
        self.btn_autovectores.setEnabled(True)
        self.btn_graficaVarianza.setEnabled(True)
        self.btn_graficaPCA.setEnabled(True)
        self.btn_KNN.setEnabled(True)
        self.btn_naiveBayes.setEnabled(True)
        self.btn_neuralNetwork.setEnabled(True)
        self.label_tituloClasifi.setText("Elige uno de los clasificadores")
        
    
        
    def btn_matriz_cov(self):
        self.label_tituloPCA.setText("Matriz de Covarianza")
        self.plain_resultadoPCA.setPlainText("\n\n"+str_cov_mat)
        
    def btn_autVal(self):
        self.label_tituloPCA.setText("Autovalores de la matriz")
        self.plain_resultadoPCA.setPlainText("\n\n"+str_eig_vals)
        
        
    def btn_autVec(self):
        self.label_tituloPCA.setText("Autovectores de la matriz")
        self.plain_resultadoPCA.setPlainText("\n"+str_eig_vecs)
        
    def btn_graficaVar(self):
        global num_elementos
        global num_atributos
        global num_clases
        with plt.style.context('seaborn-pastel'):
            plt.figure(figsize=(8, 8))

            plt.bar(range(num_atributos), var_exp, alpha=0.5, align='center',
                    label='Varianza individual explicada', color='g')
            plt.step(range(num_atributos), cum_var_exp, where='mid', linestyle='--', label='Varianza explicada acumulada')
            plt.ylabel('Ratio de Varianza Explicada')
            plt.xlabel('Componentes Principales')
            plt.legend(loc='best')
            plt.tight_layout()
            plt.grid()
            plt.show()
    
    
    def btn_grafPCA(self):
        import matplotlib
        import random
        colores = []
        colores_random = []
        clases = []
        #Obtener todos los colores en de la bibliotea matplotlib
        for name, hex in matplotlib.colors.cnames.items():
            colores.append(str(name))
        for i in range(num_clases):
            clases.append(i)
            colores_random.append(random.choice(colores))
        matrix_w = np.hstack((eig_pairs[0][1].reshape(num_atributos,1),
                      eig_pairs[1][1].reshape(num_atributos,1)))
        Y = X_std.dot(matrix_w)
        with plt.style.context('seaborn-whitegrid'):
            plt.figure(figsize=(8,8))
            for lab, col in zip(clases,
                                (colores_random)):
                plt.scatter(Y[y==lab, 0],
                            Y[y==lab, 1],
                            label=lab,
                            c=col)
            plt.xlabel('Componente Principal 1')
            plt.ylabel('Componente Principal 2')
            plt.legend(loc='lower center', title="Clases")
            plt.tight_layout()
            plt.grid(True)
            plt.show()
    
    
    def btn_habilitarKNN(self):
        self.btn_calcularNeural.hide()
        self.spinBox_epocas.hide()
        self.doubleSpinBox_momentum.hide()
        self.label_momentum.hide()
        self.label_4.hide()
        self.btn_calcularKNN.show()
        self.spinBox_numKNN.show()
        self.label_3.show()
        self.plain_resultClasifi.clear()
        self.label_tituloClasifi.setText("Clasificador 'Vecinos más Cercanos' seleccionado")
        w=self.label_imagenClasifi.width()
        h=self.label_imagenClasifi.height()
        pixmap = QPixmap("Recursos/knn.jpg")
        self.label_imagenClasifi.setPixmap(pixmap.scaled(w,h));
        
    def btn_calcKNN(self):
        global num_elementos
        global num_atributos
        global num_clases
        global model_1
        global score_KNN
        global X_train
        global X_test
        global y_train
        global y_test
        num_vecinos = self.spinBox_numKNN.value() #obtner el numero de vecinos
        model_1 = KNeighborsClassifier(n_neighbors=num_vecinos) #crear modelo 1
        model_1.fit(X_train, y_train)
        y_pred = model_1.predict(X_test) #obtener la predicción
        matriz_confusion=confusion_matrix(y_test, y_pred) #obtener la matriz de confusión
        #Convertir un array en string y poder mostrarlo en un plainText
        str_matriz_confusion=np.array2string(matriz_confusion, formatter={'float_kind':lambda matriz_confusion: "%.10f" % matriz_confusion})
        print(classification_report(y_test, y_pred))
        self.plain_resultClasifi.setPlainText("    Utilizando "+str(num_vecinos)+" vecino(s) más cercanos para la clasificación\n\n\n"+"-----Reporte de la clasificacion-----\n\n\n"+classification_report(y_test, y_pred)+"\n\n\n-----Matriz de confusión------\n\n\n"+str_matriz_confusion+"\n\n\n-----Precisión-----\n\n"+str(accuracy_score(y_test, y_pred)))
        score_KNN = str(accuracy_score(y_test, y_pred))
        self.btn_errorKNN.setEnabled(True)
        self.btn_calcularErrorKNN(X_train, X_test, y_train, y_test, num_vecinos)

    def btn_calcularErrorKNN(self, X_train, X_test, y_train, y_test, i):
        error = []

        # Calcular error para los valores de K entre 1 y 40
        for i in range(1, 40):
            knn = KNeighborsClassifier(n_neighbors=i)
            knn.fit(X_train, y_train)
            pred_i = knn.predict(X_test)
            error.append(np.mean(pred_i != y_test))
        plt.figure(figsize=(12, 6))
        plt.plot(range(1, 40), error, color='red', linestyle='dashed', marker='o',
                 markerfacecolor='blue', markersize=10)
        plt.title('Tasa de Error por valor K')
        plt.xlabel('K')
        plt.ylabel('Error promedio')
        plt.grid()
        plt.show()
                
    def btn_calcularNaive(self):
        global num_elementos
        global num_atributos
        global num_clases
        global model_2
        global score_NB
        global X_train
        global X_test
        global y_train
        global y_test
        self.btn_calcularKNN.hide()
        self.btn_calcularNeural.hide()
        self.spinBox_numKNN.hide()
        self.spinBox_epocas.hide()
        self.doubleSpinBox_momentum.hide()
        self.label_momentum.hide()
        self.label_4.hide()
        self.btn_errorKNN.hide()
        self.label_3.hide()
        self.plain_resultClasifi.clear()
        self.label_tituloClasifi.setText("Clasificador 'Naive Bayes' seleccionado")
        w=self.label_imagenClasifi.width()
        h=self.label_imagenClasifi.height()
        pixmap = QPixmap("Recursos/naiveBayes.jpg")
        self.label_imagenClasifi.setPixmap(pixmap.scaled(w,h)); #poner la imagen correspondiente
        model_2 = GaussianNB().fit(X_train, y_train) #crear el modelo 2
        y_pred = model_2.predict(X_test) #obtener la predicción
        matriz_confusion=confusion_matrix(y_test, y_pred) #obtener la matriz de confusión
        #mostrar los resultados:
        str_matriz_confusion=np.array2string(matriz_confusion, formatter={'float_kind':lambda matriz_confusion: "%.10f" % matriz_confusion})
        self.plain_resultClasifi.setPlainText("-----Reporte de la clasificación-----\n\n\n"+classification_report(y_test, y_pred)+"\n\n\n-----Matriz de confusión------\n\n\n"+str_matriz_confusion+"\n\n\n-----Precisión-----\n\n"+str(accuracy_score(y_test, y_pred)))
        score_NB = str(accuracy_score(y_test, y_pred))
        
        
    def btn_neural(self):
        self.btn_calcularNeural.show()
        self.spinBox_epocas.show()
        self.doubleSpinBox_momentum.show()
        self.label_momentum.show()
        self.label_4.show()
        self.btn_calcularKNN.hide()
        self.spinBox_numKNN.hide()
        self.label_3.hide()
        self.plain_resultClasifi.clear()
        self.label_tituloClasifi.setText("Clasificador 'Redes Neuronales' seleccionado")
        w=self.label_imagenClasifi.width()
        h=self.label_imagenClasifi.height()
        pixmap = QPixmap("Recursos/NN2.png")
        self.label_imagenClasifi.setPixmap(pixmap.scaled(w,h)); #poner la imagen correspondiente
    

    def btn_calcularNN(self):
        global num_elementos
        global num_atributos
        global num_clases
        global model_3
        global score_NN
        global X_train
        global X_test
        global y_train
        global y_test
        num_epocas = self.spinBox_epocas.value() #obtner el número de épocas
        mom_tum = self.doubleSpinBox_momentum.value() #obtener momentum
        model_3 = MLPClassifier(hidden_layer_sizes=(13,13,13), max_iter=num_epocas, warm_start=True, momentum=mom_tum) #crear el modelo 3
        model_3.fit(X_train, y_train) 
        y_pred = model_3.predict(X_test)#obtener la predicción
        matriz_confusion=confusion_matrix(y_test, y_pred) #obtener la matriz de confusión
        #mostrar los resultados:
        str_matriz_confusion=np.array2string(matriz_confusion, formatter={'float_kind':lambda matriz_confusion: "%.10f" % matriz_confusion})
        self.plain_resultClasifi.setPlainText("Número de épocas: "+str(num_epocas)+"\n\nMomentum: "+str(mom_tum)+"\n\n\n-----Reporte de la clasificación-----\n\n\n"+classification_report(y_test, y_pred)+"\n\n\n-----Matriz de confusión------\n\n\n"+str_matriz_confusion+"\n\n\n-----Precisión-----\n\n"+str(accuracy_score(y_test, y_pred)))
        score_NN = str(accuracy_score(y_test, y_pred))
     
    def btn_calcularEnsamble(self):
        if(score_KNN=="" and score_NB=="" and score_NN==""):
            self.label_tituloEnsamble.setText("No se han calculado ninguno de los tres clasificadores")
        elif(score_KNN=="" and score_NB==""):
            self.label_tituloEnsamble.setText("No se ham calculado los clasificadores KNN y Naive Bayes")
        elif(score_NB=="" and score_NN==""):   
            self.label_tituloEnsamble.setText("No se han calculado los clasificadores Naive Bayes y Neural Network")
        elif(score_KNN=="" and score_NN==""):
            self.label_tituloEnsamble.setText("No se han calculado los clasificadores KNN y Neural Network")
        elif(score_KNN==""):
            self.label_tituloEnsamble.setText("No se ha calculado el clasificador KNN")
        elif(score_NB==""):
            self.label_tituloEnsamble.setText("No se ha calculado el clasificador Naive Bayes")
        elif(score_NN==""):
            self.label_tituloEnsamble.setText("No se ha calculado el clasificador Neural Network")
        else:
            self.label_tituloEnsamble.setText("Ensamble calculado correctamente")
            self.label_KNN.setText("Precisión KNN: "+score_KNN)
            self.label_NB.setText("Precisión NB: "+score_NB)
            self.label_NN.setText("Precisión NN: "+score_NN)
            
            #crear el diccionario de con los tres modelos
            estimators=[('knn', model_1), ('nb', model_2), ('nn', model_3)]
            #crear el ensamble por votación, ingresando los modelos
            ensamble = VotingClassifier(estimators, voting='hard')
            #ajustar el modelo a los datos de entrenamiento
            ensamble.fit(X_train, y_train)
            #probar el ensamble
            ensamble.score(X_test, y_test)
          
            #Calcular predicción
            y_pred = ensamble.predict(X_test)
            matriz_confusion=confusion_matrix(y_test, y_pred) #obtener la matriz de confusión
            #mostrar los resultados:
            str_matriz_confusion=np.array2string(matriz_confusion, formatter={'float_kind':lambda matriz_confusion: "%.10f" % matriz_confusion})
            self.plain_resultEnsam.setPlainText("-----Reporte del ensamble-----\n\n\n"+classification_report(y_test, y_pred)+"\n\n\n-----Matriz de confusión------\n\n\n"+str_matriz_confusion+"\n\n\n-----Precisión-----\n\n"+str(accuracy_score(y_test, y_pred)))
            
    
        
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    GUI = EjGUI()
    GUI.show()
    #plt.grid(True)
    sys.exit(app.exec_())